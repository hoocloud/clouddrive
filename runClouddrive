#!/usr/bin/env python3
import sys
import os
import logging
import argparse
import subprocess

scriptpath = sys.argv[0]
scriptname = os.path.basename(scriptpath)
progpath = os.path.dirname(os.path.abspath(scriptpath))
progname = os.path.basename(progpath)

logging.basicConfig(level=logging.NOTSET, 
                    format = '%(asctime)s.%(msecs)03d %(levelname)s %(module)s '
                             '%(funcName)s: %(message)s',
                    datefmt = '%Y-%m-%d %H:%M:%S')
log=logging.getLogger()


def main():
    global scriptpath
    global scriptname
    global progpath
    global progname 
    log.debug('scriptpath: \"{}\"'.format(scriptpath))
    log.debug('scriptname: \"{}\"'.format(scriptname))
    log.debug('progpath: \"{}\"'.format(progpath))
    log.debug('progname: \"{}\"'.format(progname))

    parser = argparse.ArgumentParser(prog = progname)
    parser.add_argument('--project', 
                        default = 'default',
                        dest =  'project',
                        help = ('the project'),
                       )
    parser.add_argument('--multibuild', 
                        action="store_true",
                        dest =  'multibuild',
                        help = ('iterate over all folders in the project cfg dir'),
                       )
    args = parser.parse_args()

    project=args.project
    multibuild=args.multibuild

    log.debug('removing container clouddrive-running-{}'.format(project))

    subprocess.run(['podman', 'rm',  '-if',  'clouddrive-running-{}'.format(project)])

    log.debug('starting new clouddrive-running-{}'.format(project))

    if not 'CLOUDDRIVE_CFG_DIR' in os.environ:
        os.environ['CLOUDDRIVE_CFG_DIR'] = '/srv/containers/prj/{}/{}/cfg'.format(project, progname)
    if not os.path.isdir(os.environ['CLOUDDRIVE_CFG_DIR']):
        log.debug('CLOUDDRIVE_CFG_DIR directory {} does not exist - exiting'.format(os.environ['CLOUDDRIVE_CFG_DIR']))
        sys.exit(1)
    log.debug('CLOUDDRIVE_CFG_DIR "{}"'.format(os.environ.get('CLOUDDRIVE_CFG_DIR')))

    if not 'CLOUDDRIVE_OUT_DIR' in os.environ:
        os.environ['CLOUDDRIVE_OUT_DIR'] = '/srv/containers/prj/{}/{}/data'.format(project, progname)
    os.makedirs(os.environ['CLOUDDRIVE_OUT_DIR'], mode=0o755, exist_ok=True)

    log.debug('CLOUDDRIVE_OUT_DIR "{}"'.format(os.environ.get('CLOUDDRIVE_OUT_DIR')))

    subprocess.run(['podman', 'run',
                    '--rm',
                    '--name', 'clouddrive-running-{}'.format(project),
                    '--volume', '{}:/cfg'.format(os.environ.get('CLOUDDRIVE_CFG_DIR')),
                    '--volume', '{}:/out'.format(os.environ.get('CLOUDDRIVE_OUT_DIR')), 
                    'clouddrive:latest'
                   ])

if __name__ == '__main__':
    sys.exit(main())
